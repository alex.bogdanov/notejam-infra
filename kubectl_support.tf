data "template_file" "kubeconfig" {
  template = <<EOF
apiVersion: v1
current-context: ${var.cluster_name}
kind: Config
preferences: {}
clusters:
- name: ${var.cluster_name}
  cluster:
    certificate-authority-data: ${google_container_cluster.primary.master_auth.0.cluster_ca_certificate}
    server: https://${google_container_cluster.primary.endpoint}
users:
- name: ${local.cluster_username}
  user:
    client-certificate-data: ${google_container_cluster.primary.master_auth.0.client_certificate}
    client-key-data: ${google_container_cluster.primary.master_auth.0.client_key}
    password: ${local.cluster_password}
    username: ${local.cluster_username}
- name: ${google_container_cluster.primary.master_auth.0.username}-basic-auth
  user:
    password: ${local.cluster_password}
    username: ${local.cluster_username}
contexts:
- name: ${var.cluster_name}
  context:
    cluster: ${var.cluster_name}
    user: ${local.cluster_username}
EOF
}

resource "local_file" "kubeconfig" {
  content     = "${data.template_file.kubeconfig.rendered}"
  filename = "${path.module}/kubeconfig"
}
