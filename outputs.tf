output "google_project" {
  value = "${var.google_project}"
}
output "google_region" {
  value = "${var.region}"
}

output "cluster_name" {
  value = "${var.cluster_name}"
}

output "root_kubeconfig" {
  value = "\n${data.template_file.kubeconfig.rendered}"
}

output "cluster_ui" {
  value = "https://console.cloud.google.com/kubernetes/clusters/details/${var.region}-${var.cluster_zone}/${var.cluster_name}?project=${var.google_project}"
}
