resource "google_dns_managed_zone" "cluster_zone" {
  name = "${replace(var.base_dns, ".","-")}"
  dns_name = "${var.base_dns}."
}

resource "google_dns_record_set" "asterisk" {
  managed_zone = "${google_dns_managed_zone.cluster_zone.name}"
  name    = "*.${var.base_dns}."
  type    = "CNAME"
  ttl     = "300"
  rrdatas = ["${var.base_dns}."]
}

data "template_file" "extdns" {
  template = <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: external-dns
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: external-dns
rules:
- apiGroups: [""]
  resources: ["services"]
  verbs: ["get","watch","list"]
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["get","watch","list"]
- apiGroups: ["extensions"]
  resources: ["ingresses"]
  verbs: ["get","watch","list"]
- apiGroups: [""]
  resources: ["nodes"]
  verbs: ["list"]
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: external-dns-viewer
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: external-dns
subjects:
- kind: ServiceAccount
  name: external-dns
  namespace: kube-system
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: external-dns
spec:
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: external-dns
    spec:
      serviceAccountName: external-dns
      containers:
      - name: external-dns
        image: registry.opensource.zalan.do/teapot/external-dns:v0.5.5
        args:
        # enabling only service beacuse L7 Ingresses are handled by traefik via asterisk-to-apex cname
        - --source=service
        - --domain-filter=${var.base_dns}
        - --provider=google
        - --google-project=${var.google_project}
        - --registry=txt
        - --txt-owner-id=external-dns-${var.cluster_name}
EOF
}

resource "null_resource" "kubectl_apply_external_dns" {
  triggers = {
    definition = "${data.template_file.extdns.rendered}",
    cluster    = "${google_container_cluster.primary.endpoint}"
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.extdns.rendered}' | kubectl --kubeconfig ${path.module}/kubeconfig --namespace kube-system apply -f -"
  }
}
