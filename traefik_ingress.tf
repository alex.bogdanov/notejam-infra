locals {
  basicauth_str = "${local.cluster_username}:${bcrypt(local.cluster_password)}"
}

data "template_file" "ingress_controller_definition" {
  template = <<EOF
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: traefik-ingress-controller
rules:
  - apiGroups:
      - ""
    resources:
      - services
      - endpoints
      - secrets
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - extensions
    resources:
      - ingresses
    verbs:
      - get
      - list
      - watch
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: traefik-ingress-controller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: traefik-ingress-controller
subjects:
- kind: ServiceAccount
  name: traefik-ingress-controller
  namespace: kube-system
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: traefik-ingress-controller
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: traefik-conf
data:
  traefik.toml: |
    defaultEntryPoints = ["http","https"]
    [entryPoints]
      [entryPoints.http]
      address = ":80"
      [entryPoints.https]
      address = ":443"
      [entryPoints.https.tls]
    [acme]
    email = "${local.cluster_username}@${var.base_dns}"
    storage = "/acme/acme.json"
    entryPoint = "https"
    onHostRule = true
    #caServer = "https://acme-staging-v02.api.letsencrypt.org/directory"
    [acme.httpChallenge]
      entryPoint = "http"
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: traefik-acme
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: standard
  resources:
    requests:
      storage: 512Mi
---
kind: DaemonSet
apiVersion: extensions/v1beta1
metadata:
  name: traefik-ingress-controller
  labels:
    k8s-app: traefik-ingress-lb
spec:
  template:
    metadata:
      labels:
        k8s-app: traefik-ingress-lb
        name: traefik-ingress-lb
    spec:
      serviceAccountName: traefik-ingress-controller
      terminationGracePeriodSeconds: 60
      volumes:
        - name: config
          configMap:
            name: traefik-conf
        - name: acme
          persistentVolumeClaim:
            claimName: traefik-acme
          #hostPath:
          #  type: DirectoryOrCreate
          #  path: /var/lib/k8s/acme
      containers:
      - image: traefik
        name: traefik-ingress-lb
        volumeMounts:
        - mountPath: "/config"
          name: "config"
        - mountPath: "/acme"
          name: "acme"
        ports:
        - name: http
          containerPort: 80
        - name: https
          containerPort: 443
        - name: admin
          containerPort: 8080
        securityContext:
          capabilities:
            drop:
            - ALL
            add:
            - NET_BIND_SERVICE
        args:
        - --configfile=/config/traefik.toml
        - --api
        - --kubernetes
        - --logLevel=INFO
---
kind: Service
apiVersion: v1
metadata:
  name: traefik-ingress-service
  namespace: kube-system
  annotations:
    external-dns.alpha.kubernetes.io/hostname: ${var.base_dns}
spec:
  selector:
    k8s-app: traefik-ingress-lb
  ports:
    - protocol: TCP
      port: 80
      name: http
    - protocol: TCP
      port: 443
      name: https
  type: LoadBalancer #this creates an L4

---
apiVersion: v1
kind: Service
metadata:
  name: traefik-web-ui
spec:
  selector:
    k8s-app: traefik-ingress-lb
  ports:
  - name: admin
    port: 80
    targetPort: 8080
---
apiVersion: v1
kind: Secret
metadata:
  name: traefik-web-ui-secret
type: Opaque
data:
  basicauth: ${base64encode(local.basicauth_str)}
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: traefik-web-ui
  annotations:
    kubernetes.io/ingress.class: "traefik"
    traefik.ingress.kubernetes.io/auth-type: "basic"
    traefik.ingress.kubernetes.io/auth-secret: "traefik-web-ui-secret"
    traefik.ingress.kubernetes.io/redirect-entry-point: https
spec:
  rules:
  - host: traefik.${var.base_dns}
    http:
      paths:
      - path: /
        backend:
          serviceName: traefik-web-ui
          servicePort: 80
EOF
}

resource "null_resource" "kubectl_apply_ingress_controller" {
  triggers = {
    definition = "${data.template_file.ingress_controller_definition.rendered}",
    cluster    = "${google_container_cluster.primary.endpoint}"
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.ingress_controller_definition.rendered}' | kubectl --kubeconfig ${path.module}/kubeconfig apply --namespace kube-system -f -"
  }
}
