Notejam Infrastructure
===

Setting up from scratch:
1. Create a new google cloud project
1. [Create a new GCS Bucket](https://console.cloud.google.com/storage/browser) to keep infrastructure state in
1. Open [GitLab CI Settings](https://gitlab.com/alex.bogdanov/notejam-infra/settings/ci_cd)
1. Configure `GOOGLE_PROJECT` variable to your project ID
1. Configure `TERRAFORM_BACKEND_BUCKET` variable to your state bucket name

1. In debian-based linux terminal:
```
echo "deb http://packages.cloud.google.com/apt cloud-sdk-$(lsb_release -c -s) main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get install google-cloud-sdk jq

gcloud login #this opens a new browser session

serviceaccount_email=$(gcloud iam service-accounts create gitlab-ci --project "$GOOGLE_PROJECT" --format json | jq -r '.email')
gcloud projects add-iam-policy-binding "$GOOGLE_PROJECT" --member "serviceAccount:$serviceaccount_email" --role roles/owner
gcloud iam service-accounts keys create --iam-account "$serviceaccount_email" serviceaccount_credentials.json

cat serviceaccount_credentials.json
```

1. Use the resulting JSON to configure a `GOOGLE_CREDENTIALS` variable in [GitLab CI Settings](https://gitlab.com/alex.bogdanov/notejam-infra/settings/ci_cd)
1. Run [Deploy pipeline](https://gitlab.com/alex.bogdanov/notejam-infra/pipelines)

1. You can now access the cluster by the link that was printed at the end of deployment job
