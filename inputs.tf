variable "google_project" {
  default = "toptal-screening-abogdanov"
}
variable "region" {
  default="us-central1"
}
variable "cluster_name" {
  default = "toptal-screening"
}
variable "base_dns" {
  default = "toptal-screening.syn.im"
}
variable "dns_zone" {
  default = "toptal-screening.syn.im"
}
variable "cluster_initial_size" {
  default = 1
}
variable "cluster_zone" {
  default = "a"
}
variable "node_type" {
  default = "g1-small"
}

resource "random_id" "k8s_password" {
  keepers = {
    # Generate a new id each time we switch domain
    value = "${var.cluster_name}"
  }
  byte_length = 12
}

provider "google" {
  project     = "${var.google_project}"
  region      = "${var.region}"
  zone        = "${var.region}-${var.cluster_zone}"
}
