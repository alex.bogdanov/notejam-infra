locals {
  cluster_username = "root"
  cluster_password = "${random_id.k8s_password.b64}"
}

resource "google_container_cluster" "primary" {
  name               = "${var.cluster_name}"
  initial_node_count = "${var.cluster_initial_size}"
  min_master_version = "1.10.6-gke.2"

  master_auth {
    username = "${local.cluster_username}"
    password = "${local.cluster_password}"
  }

  addons_config {
    horizontal_pod_autoscaling {
      disabled = true
    }
    http_load_balancing {
      disabled = false
    }
    kubernetes_dashboard {
      disabled = true
    }
    network_policy_config {
      disabled = true
    }
  }

  node_config {
    machine_type = "${var.node_type}"
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/ndev.clouddns.readwrite",
    ]
  }
}
